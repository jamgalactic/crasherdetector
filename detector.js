const analyzer = require('./src/analyzer');


/**
 * @typedef videoInfo
 * @property {string} video - the video url
 * @property {Array} info - array of stream info
 * @property {boolean} crasher - is the video identified as a crasher
 * @property {string} md5 - md5 hash of file
 */

/**
 * Analyzes video at the specified URL, if valid
 * @param {string} videoUrl 
 * @param {boolean} generateMd5 
 * @returns {videoInfo} videoInfo
 */
async function analyzeVideo(videoUrl, generateMd5) {
    let info = await analyzer(videoUrl, true, generateMd5).catch((err) => {
        return Promise.reject(err);
    });
    return Promise.resolve(info);
}

const Detector = {
    AnalyzeVideo: analyzeVideo
}

module.exports = Detector;