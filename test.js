const Detector = require('./detector');

let path = 'https://media1.tenor.com/images/a383413b86da9791fdb09eee34ccc519/tenor.gif?itemid=22082529';

var test = async (url) => {
    let analyze = await Detector.AnalyzeVideo(url, true).catch((err) => {
        console.log(err);
        return;
    });
    if(analyze)
        console.log(analyze);
    return;
};

test(path);