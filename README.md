# Discord Crasher Detector
## _Check if a Video File will Crash the Discord Desktop Client_

## How It Works
Video URLs are first tested to see if they direct to a valid video file.
If the url provided is a gfycat url, we request the video page from gfycat and extract the valid video url;
The video is then analyzed using ffprobe-static, a static package with ffmpeg's ffprobe binaries
If the video has properties that match those that crash discord, the analysis returns `crasher: true`

## Installation
`npm install discord-crasher-detector`

## Method
### Detector.AnalyzeVideo
Analyzes video file at the specified URL
| Parameter | Type | Optional | Default | Description |
| --------- | ---- | -------- | ------- | ----------- |
| videoUrl  | string |   | *none* | The URL of the Video |
| generateMd5 | boolean | ✓ | false |  Include MD5 checksum for the video |
**Returns:** `Promise<videoInfo>`

## Example Usage
```
const Detector = require('discord-crasher-detector');

let videoUrl = 'https://thumbs.gfycat.com/bluedampatlanticridleyturtle';
let analyze = Detector.AnalyzeVideo(videoUrl, true);
analyze.then((res) => {
    console.log(res);
}, (rej) => {
    console.log(rej);
});
```
## Async/Await Support
```
const Detector = require('discord-crasher-detector');

async function runDetector(videoUrl) {
    let analysis = await Detector.AnalyzeVideo(videoUrl, true).catch((err) => {
        console.log(err);
    }
    if(analysis)
      console.log(analysis);
}
runDetector('https://thumbs.gfycat.com/bluedampatlanticridleyturtle');
```
**Example Result**
```
{
  video: 'https://giant.gfycat.com/BlueDampAtlanticridleyturtle.mp4',
  info: [
    {
      index: 0,
      codec_name: 'h264',
      codec_long_name: 'H.264 / AVC / MPEG-4 AVC / MPEG-4 part 10',
      profile: 'High 4:2:2',
      codec_type: 'video',
      codec_time_base: '457/36864',
      codec_tag_string: 'avc1',
      codec_tag: '0x31637661',
      width: 420,
      height: 420,
      coded_width: 432,
      coded_height: 432,
      has_b_frames: 2,
      sample_aspect_ratio: '1:1',
      display_aspect_ratio: '1:1',
      pix_fmt: 'yuv422p',
      level: 31,
      chroma_location: 'left',
      refs: 1,
      is_avc: 'true',
      nal_length_size: '4',
      r_frame_rate: '60/1',
      avg_frame_rate: '18432/457',
      time_base: '1/15360',
      start_pts: 0,
      start_time: '0.000000',
      duration_ts: 4577,
      duration: '0.297982',
      bit_rate: '2798518',
      bits_per_raw_sample: '8',
      nb_frames: '12',
      disposition: [Object],
      tags: [Object]
    }
  ],
  crasher: true,
  md5: '44c6dd82288a42f20f1cfce1331abfb4'
}
```