const got = require('got');
const crypto = require('crypto');
const url = require('url');
const FileType = require('file-type');
const VideoProbe = require('./videoprobe');
const cheerio = require('cheerio');

const URL = url.URL;

/**
 * Analyzes video file at the specified URL and returns Video Info.
 * @param {string} videoUrl - the video url
 * @param {boolean} checkForCrasher - scans video for crasher properties if true
 * @param {boolean} generateMd5 - includes a generated MD5 checksum for the video
 * @returns {videoInfo}
 */
async function getVideoInfo(videoUrl, checkForCrasher = false, generateMd5 = false) {
    let gifHost = gifHosts.some((h) => {
            return videoUrl.indexOf(h) >= 0;
    });
    if(gifHost) {
        videoUrl = await getGifHostVideoUrl(videoUrl);
    }
    let validateFileType = await getFileType(videoUrl).catch((err) => {
        return Promise.reject(err);
    });
    if(validateFileType && validateFileType.type === 'video') {
        let vinfo = await videoInfo(videoUrl).catch((err) => {
            return Promise.reject(err);
        });
        let results = {
            video: videoUrl,
            info: vinfo
        };
        if(checkForCrasher){
            let crasher = await videoIsCrasher(videoUrl).catch((err) => {
                return Promise.reject(err);
            })
            results.crasher = crasher;
        }
        if(generateMd5) {
            let md5 = await generateMd5HashFromUrl(videoUrl).catch((err) => {
                return Promise.reject(err);
            });
            results.md5 = md5;
        }
        return Promise.resolve(results);
    } else {
        return Promise.reject('Invalid FileType. File must be a video file');
    }
}

/**
 * 
 * @param {string} url 
 * @returns {object}
 */
async function getFileType(url){
    return new Promise((res, rej) => {
        let videoUrl = undefined;
        try{
            videoUrl = new URL(url);
        } catch(err) {
            rej(err);
        }
    
        let stream = got.stream(videoUrl.href).on('error', (err) => {    
            rej(err.response.statusCode);
        });
    
        if(!stream) return Promise.reject(`Failed to open stream: ${videoUrl.href}`);
    
        let fileType = FileType.fromStream(stream);
        fileType.then((ft) => {
            if(ft) {
                let {mime} = ft;
                let [type, format] = mime.split('/');
                res({type: type, format: format});
            } else {
                if(url.match(fileExtensionsRegex.video) != null)
                    res({type: 'video'});
                else if (url.match(fileExtensionsRegex.image) != null)
                    res({type: 'image'});
                else if (url.match(fileExtensionsRegex.audio) != null)
                    res({type: 'audio'});
                else 
                    res({type: undefined});
            }
        }, (err) => {
            rej(err);
        });
    });
}

/**
 * 
 * @param {string} videoUrl 
 * @returns 
 */
async function videoIsCrasher(videoUrl){
    let frames = await VideoProbe.GetFrames(videoUrl).catch((err) => {
        return Promise.reject(err);
    });
    let crasher = await isCrasher(frames).catch((err) => {
        return Promise.reject(err);
    });
    return Promise.resolve(crasher);
}

/**
 * @typedef videoInfo
 * @property {string} video - the video url
 * @property {Array} info - array of stream info
 * @property {boolean} crasher - is the video identified as a crasher
 * @property {string} md5 - md5 hash of file
 */

/**
 * Get video metadata from url
 * @param {string} videoUrl 
 * @returns {object}
 */
async function videoInfo(videoUrl){
    let info = await VideoProbe.GetInfo(videoUrl).catch((err) => { return Promise.reject(err);});
    return Promise.resolve(info.streams);
}

/**
 * checks if video is a discord crasher by looking for resolution changes
 * @param {string} videoInfo 
 * @returns {boolean}
 */
async function isCrasher(videoInfo){
    let first = videoInfo.frames[0];
    let resolutionChanges = videoInfo.frames.some(f => f.width !== first.width || f.height !== first.height);        
    return Promise.resolve(resolutionChanges);
}

/**
 * Generate md5 checksum from file stream
 * @param {string} filePath 
 * @returns {Promise}
 */
function generateMd5HashFromUrl(filePath){
    return new Promise(async (res, rej) => {
        try{
            let testRequest = await got(filePath).catch((err) => {
                rej(err.response.statusCode);
            });
            var md5 = crypto.createHash('md5').setEncoding('hex');
            var urlStream = got.stream(filePath).on('error', (err) => {
                rej(err.response.statusCode);
            });
            urlStream.pipe(md5).on('error', (err) => {
                rej(err.response.statusCode);
            }).on('finish', () => {
                res(md5.read());
            });
        } catch(err) {
            rej(err);
        }
    });
}

/**
 * Gets the valid mp4 url from gfycat
 * @param {string} gifHostUrl 
 * @returns 
 */
async function getGifHostVideoUrl(gifHostUrl){
    let properUrl = gifHostUrl;

    if(gifHostUrl.indexOf('https://media1.tenor.com') >= 0) {
        let idindex = gifHostUrl.indexOf('tenor.gif?itemid=') + 17;
        if(idindex >= 0) {
            let id = gifHostUrl.substr(idindex, gifHostUrl.length - idindex);
            properUrl = `https://tenor.com/view/gif-${id}`;
            console.log(properUrl);
        }
    } else if(gifHostUrl.indexOf('https://thumbs.gfycat.com') >= 0 || gifHostUrl.indexOf('https://giant.gfycat.com') >= 0) {
        properUrl = gifHostUrl.replace('thumbs.gfycat.com', 'gfycat.com').replace('giant.gfycat.com', 'gfycat.com').replace('.mp4','');
    }
    let response = await got(properUrl).catch((err) => {
        return Promise.reject(err.response.body);
    });
    try {
        const $ = cheerio.load(response.body);
        let scrapeScript = $('script[type="application/ld+json"]').html();
        let videoContext = JSON.parse(scrapeScript);
        let validLink = videoContext.video.contentUrl;
        return Promise.resolve(validLink);
    } catch(err) {
        return Promise.reject(err);
    }
}

const fileExtensionsRegex = {
    video: /\.(webm|mpg|mp2|mpeg|mpe|mpv|mp4|m4p|m4v|avi|wmv|mov|qt|flv|swf|avchd)$/,
    image: /\.(jpg|jpeg|png|webp|svg|gif)$/,
    audio: /\.(m4a|flac|mp3|wav|wma|aac|aiff|au|alac|ogg|dsd)$/
}

const gifHosts = [
    'tenor.com',
    'gfycat.com'
];

module.exports = getVideoInfo;