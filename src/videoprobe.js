const ffprobeStatic = require('ffprobe-static');
const JSONStream = require('JSONStream');
const Deferred = require('deferential');
const bl = require('bl');
const child_process = require('child_process');
const spawn = child_process.spawn;

function getVideoInfo(filePath, cb){
  var params = [];
  params.push('-show_streams', '-print_format', 'json', filePath);

  var d = Deferred();
  var info;
  var stderr;

  var ffprobe = spawn(ffprobeStatic.path, params);
  ffprobe.once('close', function (code) {
    if (!code) {
      d.resolve(info);
    } else {
      var err = stderr.split('\n').filter(Boolean).pop();
      d.reject(new Error(err));
    }
  });

  ffprobe.stderr.pipe(bl(function (err, data) {
    stderr = data.toString();
  }));

  ffprobe.stdout
    .pipe(JSONStream.parse())
    .once('data', function (data) {
      info = data;
    });

  return d.nodeify(cb);
}

function getVideoFrames(filePath, cb){
  var params = [];
  params.push('-v', 'error', '-show_entries', 'frame=pkt_pts_time,width,height', '-select_streams', 'v', '-of', 'json', filePath);

  var d = Deferred();
  var info;
  var stderr;

  var ffprobe = spawn(ffprobeStatic.path, params);
  ffprobe.once('close', function (code) {
    if (!code) {
      d.resolve(info);
    } else {
      var err = stderr.split('\n').filter(Boolean).pop();
      d.reject(new Error(err));
    }
  });

  ffprobe.stderr.pipe(bl(function (err, data) {
    stderr = data.toString();
  }));

  ffprobe.stdout
    .pipe(JSONStream.parse())
    .once('data', function (data) {
      info = data;
    });

  return d.nodeify(cb);
}

const VideoProbe = {
    GetInfo: getVideoInfo,
    GetFrames: getVideoFrames
}

module.exports = VideoProbe;